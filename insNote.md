# NCHC INS Note

Contain 3 part
+ Local Java Application
+ Database Server
+ Webserver


## Local Application

### Development Tool
+ Eclipse (Egit plguin)

### Libraries (in lib)
+ RXTX JAVA Communication API
+ JOGL
+ LOG4J
+ mongodb

## Database Server
+ [mlab](https://mlab.com/home)
+ Account: nchc
+ Password: nchcTaiwan556
+ DataBase: nchc_ins
+ DataBase User: rootins
+ DataBase password: nchcTaiwan556
+ Only **500MB** data


## Webserver
+ Url: http://140.110.17.180/ins
+ 後端部分使用類似RESTful API => in api.php
+ 讀取資料庫需安裝php mongodb driver
    + [PHP7 MongoDB driver install](http://mongodb.github.io/mongo-php-library/getting-started/#installation)
    + [MongoDB deriver Doc](http://mongodb.github.io/mongo-php-library/classes/client/)
    + [MongoDB driver Ref](http://mongodb.github.io/mongo-php-library/api/class-MongoDB.Model.CollectionInfo.html)

+ 前端部分使用工具
    + BabylonJS: static/js/Babylon
    + jquery: static/jQuery
    + Bootstrap: static/bootstrap
    
+ 3D 圖形: static/scene.js
