package tw.org.nchc.ins;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.vecmath.Point3d;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import tw.org.nchc.ins.SerialReader;

//add gluegen-rt.jar, jogl-awt.jar ,jogle-all.jar , RXTXcomm.jar , vecmath
//Properities-> Java Build Path-> Libraries -> add jar-> in lib
public class App {
	
	//logger
	public static Logger log;
	private static final boolean disable_com = false;
	public static ArrayList<Point3d> arr = new ArrayList<Point3d>();
	public static Map< Integer, StatusInfo> map_device = new LinkedHashMap<Integer , StatusInfo>();
	private final static int Fps = 10;
	final static int status_pannel_w = 400;
	final static int status_pannel_h = 900;
	public static void main(String[] args) throws FileNotFoundException {
		byte [] test_data  = new byte[] { 0x2A  ,(byte)0x23  ,(byte)0x3A  ,(byte)0xB4  ,(byte)0x9E  ,(byte)0xBB  ,(byte)0x65  ,(byte)0x0C  ,(byte)0x24  ,(byte)0x67  ,(byte)0xBA  ,(byte)0x02  ,(byte)0x00  ,(byte)0x00  ,(byte)0xDA  ,(byte)0x36  ,(byte)0xA7  ,(byte)0xBE  ,(byte)0xE0  ,(byte)0x81  ,(byte)0xC7  ,(byte)0xBE  ,(byte)0x56  ,(byte)0x87  ,(byte)0x00  ,(byte)0x00  ,(byte)0x1E  ,(byte)(byte) 0xF4  ,(byte)0xA4  ,(byte)0xBE  ,(byte)0x08  ,(byte)0xAA  ,(byte)0xC4  ,(byte)0xBE  ,(byte)0xCA  ,(byte)0xBF  ,(byte)0x00  ,(byte)0x00  ,(byte)0x58  ,(byte)0xF5  ,(byte)0xA4  ,(byte)0xBE  ,(byte)0x06  ,(byte)0x5F  ,(byte)0xC4  ,(byte)0xBE  ,(byte)0xC4  ,(byte)(byte) 0xB8  ,(byte)0x00  ,(byte)0x00  ,(byte)0x80  ,(byte)0x8F  ,(byte)0xA5  ,(byte)0xBE  ,(byte)0x4F  ,(byte)0x32  ,(byte)0xC5  ,(byte)0xBE  ,(byte)0x89  ,(byte)0xBC  ,(byte)0x64  ,(byte)0x2C  ,(byte)0x1A  ,(byte)0x02  ,(byte)0x00  ,(byte)0x91  ,(byte)0x00  ,(byte)0x38  ,(byte)0xFA  ,(byte)0xF5  ,(byte)0xF5  ,(byte)0xFA  ,(byte)0x01  ,(byte)0x4B  ,(byte)0x02  ,(byte)0x01  ,(byte)0x03  ,(byte)0x01  ,(byte)0x00  ,(byte)0x00  ,(byte)0x00  };
		
		log = LogManager.getRootLogger();
    	log.info("****************** Start Application ******************");
		
if( !disable_com ){
    		
		try{  //the USB use "COM4" fix?  may need to modify for correct port
			CommPortIdentifier portid = CommPortIdentifier.getPortIdentifier("COM4");
			
			if( portid.isCurrentlyOwned() ){
				System.out.println("Some process use the COM4\n");
				log.info("Some other process using the COM4");
				System.exit(-1);
			}
			//check the COM4 port type
			if( portid.getPortType() !=  CommPortIdentifier.PORT_SERIAL ){
				System.out.println("Port type is not serial");
				log.info("Serial port type error");
				log.info("****************** Close Application ******************");
				System.exit(-1);
			}
			SerialPort serialport = (SerialPort) portid.open("COM4", 2000); //port owner and timeout   
			//setup connection parameters
			serialport.setSerialPortParams( 115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);  
			System.out.println("COM4 setting success\n");
			InputStream in = serialport.getInputStream();
			
			//read data may using another thread 
			try{
				serialport.addEventListener( new SerialReader(in));
				serialport.notifyOnDataAvailable(true);	
				//while(true);
			}catch(Exception ex){
				System.out.println("Some error\n");
			}
		}catch(Exception ex){
			System.out.println("COM4 port Error! \ncheck com port setting\n");
			log.info("Serial port type error");
			log.info("****************** Close Application ******************");
			System.exit(-1);
			
		}
    		
}	//end for  if( !disable_com )
		
		final GLProfile profile = GLProfile.get( GLProfile.GL2 );
		GLCapabilities cap = new GLCapabilities( profile );
		final GLCanvas  glcanvas = new GLCanvas( cap );
		Scene scene = new Scene( );
		
	
		glcanvas.addGLEventListener( scene ); 
		glcanvas.addKeyListener( scene );
		glcanvas.addMouseListener(  scene );
		glcanvas.addMouseMotionListener( scene );
		glcanvas.addMouseWheelListener( scene );
		glcanvas.setSize( 800 , 800 );
		
		
		
		final JFrame frame = new JFrame("NCHC Inertial Navigation");
		
		
		//JScrollPane
		JPanel panel = new JPanel();
		DisplayPanel task = new DisplayPanel(panel);
		//panel.setLayout( new GridLayout( 0,1, 3 , 3));
		panel.setLayout( new BoxLayout( panel , BoxLayout.Y_AXIS) );
		//panel.setAlignmentX( Component.LEFT_ALIGNMENT);
		

		
		
		//panel.add(zz2);
		frame.add(panel);
		
		
		panel.setBackground( Color.GRAY);
		//panel.setMinimumSize( new Dimension( 00, 900));
		frame.add( glcanvas );
		frame.add(  panel, BorderLayout.EAST);
		//the panel size will reference the PreferSize
		panel.setPreferredSize( new Dimension( 400, 900));
		
		frame.setSize( frame.getPreferredSize() );

		frame.setVisible(true );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		frame.addWindowListener( 
				new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						log.info("****************** Close Application ******************");
					}
				});
		frame.setMinimumSize( new Dimension( 1000, 900));

if( disable_com ){
		//App.map_device.put(1706794676 , new StatusInfo(1706794676) );
		//StatusInfo info = App.map_device.get(1706794676);
		//info.setAllData(test_data , test_data.length);
}		

		

		FPSAnimator animator = new FPSAnimator( glcanvas , Fps );  
		animator.start();
		//new App().list();
		

}
	public void list() {    
        Enumeration<?> ports = CommPortIdentifier.getPortIdentifiers();     
        while(ports.hasMoreElements()){  
            CommPortIdentifier cpIdentifier = (CommPortIdentifier)ports.nextElement();  
            System.out.println(cpIdentifier.getName());              
        }  
    }
    
}
