package tw.org.nchc.ins;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.plaf.ProgressBarUI;
import javax.vecmath.Point3d;

public class DisplayPanel  extends TimerTask {
	public enum Status{
		INIT,
		NODEV,
		CONNECT,
	}
	
	
	JPanel panel;
	static int i = 0;
	JLabel [] label_device = null;
	JLabel time ;
	//int status = 0;  
	static Status status =  Status.INIT;
	static Map< Integer, StatusInfo> dev_info =  App.map_device;
	JLabel hint;
	JLabel load;
	DisplayPanel( JPanel panel){
		this.panel = panel;
		//panel.add(time);
		label_device = new JLabel [2];
		for (int j = 0; j < label_device.length; j++) {
			label_device[j] = new JLabel("");
		}
		hint = new JLabel("No device Connect" );
		hint.setFont( new Font(Font.SANS_SERIF, Font.PLAIN, 25));
		hint.setMaximumSize( new Dimension( App.status_pannel_w, 50));
		hint.setHorizontalAlignment(SwingConstants.CENTER);
		load = new JLabel( new ImageIcon("ring-alt.gif"));
		load.setHorizontalAlignment(SwingConstants.CENTER);
		load.setMaximumSize( new Dimension( App.status_pannel_w, App.status_pannel_h));
		panel.add(hint);
		panel.add(load);
		Timer t = new Timer();
		t.schedule( this , 1000 , 5000);  //period , first delay time
		
	}
	public void updateStatus(){
		
		
		if( App.map_device.isEmpty()){
			status = Status.NODEV;
			//hint.setText("No device Connect");
			return;
		}else if( hint.getParent() == panel){ //map_device is not empty
			//panel.removeAll();
			//panel.remove(hint);
			panel.removeAll();
			panel.revalidate();
			for (int j = 0; j < label_device.length; j++) {
				panel.add( label_device[j]);
			}
			panel.repaint();
			
		}
		
		int index = 0;
		for( Map.Entry<Integer, StatusInfo> entry : dev_info.entrySet() ){
			//tmp.setText(" "+entry.getValue().getid()+" "+entry.getValue().getbattery_hand()+" "+entry.getValue().getheight());
			label_device[index].setText( InfotoStr( index , entry.getValue() ));
			index++;
		}
		
			
		
	}
	
	private String InfotoStr( int index , StatusInfo info){
		String ret ;
		Point3d curpos = info.getposlist().get( info.getposlist().size()-1 ); 
		ret = "<html><span style='font-size:25px;color:blue'>Device "+index+"<span><div style='padding-left:40px'><span style='font-size:18px;'>"+
		"ID:" + info.getid() +"<br>"+ 
		"Battery(hand): "+ info.getbattery_hand() + "<br>"+
		"Battery(foot): "+ info.getbattery_foot() + "<br>"+
		"Temperature: " + info.gettemp() + "°C<br>"+
		"Height: " +info.getheight()+" m<br>"+
		"Position: "+String.format( "( %.2f , %.2f , %.2f )", curpos.x ,  curpos.y  , curpos.z)+" <br>" +
		"</span></div></html>";
		
		return ret;
	}
	public JPanel getPanel(){
		return this.panel;
	}
	@Override
	public void run() {
		updateStatus();
	}
}
