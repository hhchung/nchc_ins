package tw.org.nchc.ins;

import java.awt.Color;
import java.awt.Font;
import java.awt.datatransfer.FlavorTable;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Map;
import javax.vecmath.Point3d;


import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.jogamp.opengl.util.awt.TextRenderer;
import com.jogamp.opengl.util.gl2.GLUT;

import jogamp.graph.font.typecast.ot.table.CffTable.Index;

public class Scene extends GLCanvas implements GLEventListener , KeyListener  , MouseListener , MouseMotionListener , MouseWheelListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8695744352648210467L;
	private GLU glu;
	private GLUT glut;
	static boolean flag = false;
	float cam_x = 0.0f , cam_y = 4.0f , cam_z = -12.0f;
	int degree = 40;
	float look_x = 0.0f;
	float look_y = 0.0f;
	float look_z = 0.0f;
	int mouse_x = 0;
	int mouse_y = 0;
	int mouse_dir = 0;
	int mouse_count = 0;
	float flag_height = 1.5f; 
	
	
	
	
	int current_floor_idx = 0;
	@Override
	public void init(GLAutoDrawable drawable ) {
		final  GL2 gl =  drawable.getGL().getGL2();
		glut = new GLUT();
		drawable.setGL( new DebugGL2(  gl ));
		//gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f);  //set background color
		//gl.getwi
		//System.out.println("H:" + drawable.getSurfaceHeight() + " W:"+drawable.getSurfaceWidth() );
		gl.glHint( GL2.GL_PERSPECTIVE_CORRECTION_HINT , GL2.GL_NICEST );
		glu = new GLU();
		gl.glEnable(GL2.GL_BLEND);//for transparent
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA); //trasparent method
		gl.glEnable( GL2.GL_LINE_SMOOTH);
		gl.glHint( GL2.GL_LINE_SMOOTH_HINT, GL2.GL_NICEST);
	}
	

	//draw a grid from -half_length ~ half_length
	private void draw_grid( GL2 gl , int half_length , int grid_size , int height , float alpha ){  
		//gl.glColor3f( 0.137255f, 0.137255f , 0.556863f ); //NavyBlue
		gl.glColor4f(  0.137255f, 0.137255f , 0.556863f  , alpha);
		gl.glLineWidth(2.0f);
		gl.glBegin( GL2.GL_LINES);
		for( float start =  -half_length ; start <= half_length ;  start+= grid_size ){
			gl.glVertex3f( start , height  , -half_length  );
			gl.glVertex3f( start , height  , half_length   );
			gl.glVertex3f( -half_length , height ,  start );
			gl.glVertex3f( half_length  , height ,  start );
		}
		gl.glEnd();
	}
	private void draw_flag( GL2 gl , double posx , double posy , double posz ,  float[] color ){
		double x = posx;
		double y = posy;
		double z = posz;
		
		//float height = 1.5f;
		float s = 0.3f;//pyramid length
		//gl.glColor3f( color[0] , color[1] , color[2]);
		/*
		gl.glBegin( GL2.GL_LINES);
			gl.glVertex3d( x, y, z );
			gl.glVertex3d( x, y+height, z);
		gl.glEnd();
		*/
		
		
		gl.glColor4f( color[0] , color[1] , color[2] , 0.7f);
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_FILL);
		gl.glBegin(GL2.GL_TRIANGLES);
			

			gl.glVertex3d( x-1.732*s, y+flag_height, z+s);
			gl.glVertex3d( x, y+flag_height, z-2*s);
			gl.glVertex3d( x+1.732*s, y+flag_height, z+s);
			
			gl.glVertex3d( x, y, z);
			gl.glVertex3d( x+1.732*s, y+flag_height, z+s);
			gl.glVertex3d( x, y+flag_height, z-2*s);
			
			gl.glVertex3d( x, y, z);
			gl.glVertex3d( x-1.732*s, y+flag_height, z+s);
			gl.glVertex3d( x+1.732*s, y+flag_height, z+s);
			
			gl.glVertex3d( x, y, z);
			gl.glVertex3d( x, y+flag_height, z-2*s);
			gl.glVertex3d( x-1.732*s, y+flag_height, z+s);
			//gl.glVertex3d( x, y, z);
			//gl.glVertex3d( x+1.732*s, y+height, z+s);
			//gl.glVertex3d( x, y+height, z+s);
		gl.glEnd();
		
		gl.glLineWidth(2.0f);
		//gl.glColor3d( 0.309804 , 0.184314 , 0.184314);
		gl.glColor4d( 1.0f , 1.0f , 1.0f , 0.5d); //white line
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_LINE);
		gl.glBegin(GL2.GL_TRIANGLES);
			
			gl.glVertex3d( x-1.732*s, y+flag_height, z+s);
			gl.glVertex3d( x, y+flag_height, z-2*s);
			gl.glVertex3d( x+1.732*s, y+flag_height, z+s);
		
			gl.glVertex3d( x, y, z);
			gl.glVertex3d( x+1.732*s, y+flag_height, z+s);
			gl.glVertex3d( x, y+flag_height, z-2*s);
			
			gl.glVertex3d( x, y, z);
			gl.glVertex3d( x-1.732*s, y+flag_height, z+s);
			gl.glVertex3d( x+1.732*s, y+flag_height, z+s);
			
			gl.glVertex3d( x, y, z);
			gl.glVertex3d( x, y+flag_height, z-2*s);
			gl.glVertex3d( x-1.732*s, y+flag_height, z+s);
				
			//gl.glVertex3d( x, y, z);
			//gl.glVertex3d( x+1.732*s, y+height, z+s);
			//gl.glVertex3d( x, y+height, z+s);
		gl.glEnd();
		
	
	}
	private void draw_axis(GL2 gl){
		float offset = 0.4f; 
		float length = 5.0f;
		gl.glLineWidth(5.0f);
		gl.glBegin( GL2.GL_LINES);
			//x-axis
			gl.glColor3f( 1.0f , 0.0f, 1.0f );
			gl.glVertex3f( 0.1f , 0.0f , 0.0f );
			gl.glVertex3f( length , 0.0f , 0.0f );
			//x-axis arrow
			gl.glVertex3f( length-offset , 0.0f , offset );
			gl.glVertex3f( length , 0.0f , 0.0f );
			gl.glVertex3f( length-offset , 0.0f , -offset );
			gl.glVertex3f( length , 0.0f , 0.0f );
			
			
			//y-axis
			gl.glColor3f( 0.0f, 1.0f, 0.0f);
			gl.glVertex3f( 0.0f , 0.1f , 0.0f );
			gl.glVertex3f( 0.0f , length , 0.0f );
			//y-axis arrow
			gl.glVertex3f( offset , length-offset , 0.0f );
			gl.glVertex3f( 0.0f , length , 0.0f );
			gl.glVertex3f( -offset , length-offset , 0.0f );
			gl.glVertex3f( 0.0f , length , 0.0f );
			
			//z-axis
			gl.glColor3f(  0.0f, 0.0f, 1.0f  );
			gl.glVertex3f( 0.0f, 0.0f, 0.1f  );
			gl.glVertex3f( 0.0f, 0.0f, length);
			//z-axis arrow
			
			gl.glVertex3f( offset, 0.0f, length-offset);
			gl.glVertex3f( 0.0f , 0.0f, length);
			gl.glVertex3f( -offset, 0.0f, length-offset);
			gl.glVertex3f( 0.0f , 0.0f, length);
		gl.glEnd();
	}
	
	//final int grid_size = 1;  // 1 m
	@Override
	public void display(GLAutoDrawable drawable ) {
		
		final GL2 gl = drawable.getGL().getGL2();
		gl.glClear( GL2.GL_COLOR_BUFFER_BIT  | GL2.GL_DEPTH_BUFFER_BIT );
		
		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glPushMatrix();
		gl.glLoadIdentity();
		gl.glScalef( 1.0f, -1.0f, 1.0f);  //to invert the y-axis
		gl.glScalef( 1.0f, 1.0f, 1.0f);
		//gl.glPopMatrix();
		
		
		glu.gluLookAt(	cam_x , cam_y , cam_z,	//	eye pos
				0,0,0,	//	aim point
				0,0,-1);	//	up direction
		gl.glRotatef( degree, 0, 1, 0);
		//draw some basic objects
		draw_grid( gl , 100 , 1  , 0 , 0.2f); //grid_size:1 m
		draw_grid( gl , 80 , 1  ,  5 , 0.7f); //grid_size:1 m
		
		
		draw_axis(gl);

		//draw_flag( gl , 1.0f,8.0f,32.0f, new float[]{ 0.5f,0.5f,0.0f } );
		//draw_flag( gl , new Point3d(2.0,2.0,0.0) , new float[]{ 1.0f,0.5f,0.0f } );
				
		//move to 
		//gl.glColor3f( 1.0f, 0.0f, 0.0f);
		//gl.glRasterPos3i( 0, 5, 0);
		//glut.glutBitmapString( GLUT.BITMAP_HELVETICA_18 , "QAQQ");
	
		//TextRenderer textRenderer = TextRenderer.
		
		gl.glPointSize(10.0f);
		gl.glLineWidth( 4.0f);
		
		
		for(Map.Entry<Integer, StatusInfo> entry : App.map_device.entrySet() ){
			gl.glBegin( GL2.GL_LINE_STRIP );	//GL_LINE_STRIP
			float[] color = entry.getValue().getColor();
			gl.glColor3f( color[0], color[1], color[2]);
			int index = 0;
			for(  Point3d value : entry.getValue().getposlist() ){
				//gl.glVertex3d( value.x , 5+(int)value.y  , value.z  );  //5 to offset the height
				//gl.glVertex3d( value.x , 0  , value.z  );
				gl.glVertex3d( value.x , entry.getValue().getheightlist().get(index/4)  , value.z  );  //5 to offset the height
				index++;
			}
			gl.glEnd();
		}
		//gl.glDisable( GL2.GL_LINE_SMOOTH );
		
		
		
		
		/*
		{ 0.647059f , 0.164706f , 0.164706f },  //Brown
		{ 0.372549f , 0.623529f , 0.623529f },  //CadetBlue
		{ 0.560784f , 0.560784f , 0.737255f },  //LightSteelBlue
		{ 0.858824f , 0.439216f , 0.858824f },  //Orchid
		{ 0.917647f , 0.678431f , 0.917647f }   //Plum
		 */
		//draw_flag( gl , new Point3d(2.0,1.0,3.0) , new float[]{ 1.0f , 0.5f , 0.0f } );
		//draw_flag( gl , new Point3d(2.0,1.2,3.0) , new float[]{ 0.858824f , 0.439216f , 0.858824f } );

		//System.out.print("H  :");
		for(Map.Entry<Integer, StatusInfo> entry : App.map_device.entrySet() ){
			ArrayList<Point3d> arr = entry.getValue().getposlist();
			if( !arr.isEmpty() ){
				//System.out.println("Destination: "+arr.get( arr.size()-1 ).x +" "+ arr.get( arr.size()-1 ).y+" "+);
				//draw_flag(gl, arr.get( arr.size()-1 ) ,  new float[]{ 1.0f , 0.0f , 0.0f });
				//draw the src
				int index =  0;
				int src_h = entry.getValue().getheightlist().get(index/4).intValue();
				index = arr.size()-1;
				int dst_h = entry.getValue().getheightlist().get(index/4).intValue();
				//System.out.printf("%d ", h);
				draw_flag(gl, arr.get(0).x , src_h , arr.get(0).z ,   entry.getValue().getColor() );
				//draw the destination
				draw_flag(gl, arr.get( arr.size()-1 ).x, dst_h , arr.get( arr.size()-1 ).z ,   entry.getValue().getColor() );
				addtag( gl , arr.get( arr.size()-1 ).x , dst_h , arr.get( arr.size()-1 ).z ,    entry.getValue().getColor()  , entry.getValue().getDevName());
				
			}

		}
		//System.out.println();
		gl.glPopMatrix();
	}
	
	@Override
	public void dispose(GLAutoDrawable arg0) {
		
	}

	
	private void addtag( GL2 gl , double x , double y , double z , float[] color , String str){
		gl.glColor4f( color[0], color[1], color[2] , 1.0f);
		gl.glRasterPos3d( x, y, z);
		glut.glutBitmapString( GLUT.BITMAP_HELVETICA_18 , str);
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width , int height ) {
		// TODO Auto-generated method stub
		
		GL2 gl = drawable.getGL().getGL2();
		gl.glViewport( 0 , 0, width , height); //

		//coordinate system
		gl.glMatrixMode( GL2.GL_PROJECTION ); //set projection matrix
		gl.glLoadIdentity(); 
		
		glu.gluPerspective(60,(float)width/height,0.1,300);  //change to modify the view space
		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();
		glu.gluLookAt(	cam_x ,cam_y , cam_z ,	//eye position
				look_x, look_y, look_z,	//aim point
				0,1,0);	//up direction
	}
	


	@Override
	public void keyPressed(KeyEvent e) {
		
		switch( e.getKeyCode() ){
			case KeyEvent.VK_W:
				cam_z += 1;
				break;
			case KeyEvent.VK_A:
				cam_x -= 1;
				break;
			case KeyEvent.VK_S:
				cam_z -= 1;
				break;
			case KeyEvent.VK_D:
				cam_x += 1;
				break;
			case KeyEvent.VK_UP:
				cam_y += 1;
				break;
			case KeyEvent.VK_DOWN:
				cam_y -= 1;
				break;
			case KeyEvent.VK_LEFT:
				degree -= 10;
				break;
			case KeyEvent.VK_RIGHT:
				degree += 10;
				break;
			case KeyEvent.VK_8:
				look_z +=1;
				break;
			case KeyEvent.VK_2:
				look_z -=1;
				break;
			case KeyEvent.VK_4:
				look_x -=1;
				break;
			case KeyEvent.VK_6:
				look_x +=1;
				//System.out.println("6666");
				break;
		}
		System.out.println("CAM pos:"+cam_x+" "+cam_y+" "+cam_z+"Degree: "+degree);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		
		if( e.getModifiers() == MouseEvent.BUTTON1_MASK ){
			//System.out.println("OOOOOOOOOOOOOOOOK");
			mouse_x = e.getX();
			mouse_y = e.getY();
			System.out.println(mouse_x+" "+mouse_y);
		}
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if( e.getModifiers() == MouseEvent.BUTTON1_MASK ){
			mouse_x = e.getX();
			mouse_y = e.getY();
		}
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
		if( e.getModifiers() == MouseEvent.BUTTON1_MASK ){
			System.out.println( "Record X:"+mouse_x );
			System.out.println( "Current X:"+e.getX() );
			//System.out.println(" Modify degree:"+ (e.getX()-mouse_x) );
			if( mouse_dir == 0 ){
				if( e.getX() >= mouse_x)
					mouse_dir = 1;
				else
					mouse_dir = -1;
			}
			
			mouse_count++;
			mouse_count%=50;
			if( mouse_count == 0 ){
				mouse_x = e.getX();
				mouse_y = e.getY();
			}
			degree += (mouse_x - e.getX() )/100.0;
			cam_y  += (mouse_y - e.getY() )/100.0;
//			System.out.println( mouse_count );
			
			
			//e.get
			//mouse_x = e.getX();
			
		}
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println( e.getX()+" "+ e.getY() );
		
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		cam_z -= e.getWheelRotation();
	}

}
