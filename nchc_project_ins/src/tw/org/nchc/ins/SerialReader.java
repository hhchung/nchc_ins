package tw.org.nchc.ins;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import tw.org.nchc.ins.App;

public class SerialReader implements SerialPortEventListener {
	private enum Status{
		INIT,
		PROCESING,
		GETSTARTHEAD,
		GETENDHEAD,
		ERROR
	}
	
	private InputStream in;
    static private byte[] buffer = new byte[256];
    static int index = 0;
    static int count = 0;
    public final static int head_off = 11;
    private static  Status status = Status.INIT;
    static int data;
    private Logger log = App.log; 
    public SerialReader ( InputStream in ){
        this.in = in;
    }
    
    public void serialEvent(SerialPortEvent arg0) {
        
    	
        try{
            while ( ( data = in.read()) > -1 ){ //-1 for broken 
            	//System.out.printf( String.format("%02X ", data));
            	
            	switch( status){
	        		case INIT:
	        			if( data == 0xF5 ){
	        				status = Status.GETSTARTHEAD;
	        			}else{
	        				status = Status.ERROR;
	        			}
	        			break;
	        		case GETSTARTHEAD:
	        			if( data == 0xFA ){
	        				status = Status.PROCESING;
	        			}else{
	        				status = Status.ERROR;
	        			}
	        			break;
	        		
	        		case PROCESING:
	        			if( data == 0xFA ){
	        				status = Status.GETENDHEAD;
	        			}else{
	        				status = Status.PROCESING;
	        			}
	        			break;
	        		case GETENDHEAD:
	        			if( data == 0xF5 ){
	        				status = Status.INIT;
	        			}
	        			break;
	        		case ERROR:
	        			if( data == 0xF5){
	        				status = Status.GETSTARTHEAD;
	        				//System.out.println("QAQQQ");
	        			}
	        			break;
            	}
            	
            	
            	//System.out.println(status.toString());
            	if( status != Status.ERROR ){
            		buffer[index++] = (byte)data; 
            	}else{ //error status need to clean the buffer
            		for( int i = 0 ; i < index ; i++){
	            		buffer[i] = (byte)0;
	            	}
	            	index = 0;
            	}
	            
	            
            	if( status == Status.INIT && (index != 1)   ){ 
	            	String buffer_str="";
	        		//flush the buffer 
	        		
	        		for( int i = 0 ; i < index ; i++){
	        			buffer_str += String.format("%02X ", buffer[i])+" ";
	        		}
	        		//buffer_str += str;
	        		System.out.println( count+"\n"+buffer_str );
	        		System.out.println(buffer_str);
	        		System.out.println("Battery(F):"+buffer[61+head_off] );
	        		System.out.println("Battery(H):"+buffer[60+head_off] );
	        		log.info( buffer_str );
	        		 
	            	
	            	//System.out.println();
	            	////System.out.println(arg0);
	            	//head: 2byte , length: 1byte , id: 4byte , valid: 1byte , num: 4byte => 0~11 byte
	            	//1st step:12~23 2nd step:24~35 3rd step:36~47 4th:48~59
	            	if(  buffer[7+head_off] == 0x0C ){	
	            		int device_id = ByteBuffer.wrap( buffer , 3+head_off, 4).order( ByteOrder.LITTLE_ENDIAN).getInt(); 	
	            		System.out.println("DevID:"+device_id);
	            		//byte[] arr_data = Arrays.copyOfRange( buffer, head_off , head_off+65 );
	            		StatusInfo info = App.map_device.get(device_id);
	            		
	            		if( info == null ){ //the new device
	            			App.map_device.put(device_id , new StatusInfo(device_id) ); 
	            			info = App.map_device.get(device_id);
	            		}
	            		info.setAllData(buffer, index-head_off-1 );
	            		count++;
	        		}
            	
	            	//clear the buffer
	            	for( int i = 0 ; i < 256 ; i++){
	            		buffer[i] = (byte)0;
	            	}
	            	index = 0;

	            }
            	
            }     	
            //System.out.println("out the while loop !!!**");
        }catch ( IOException e ){
            System.out.println("ERROR:"+e.getMessage());
        	e.printStackTrace();
            System.exit(-1);
        }             
    }
}
