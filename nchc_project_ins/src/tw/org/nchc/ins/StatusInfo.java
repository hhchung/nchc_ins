package tw.org.nchc.ins;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JPanel;
import javax.vecmath.Point3d;

import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.omg.CORBA.SystemException;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;


public class StatusInfo {
	private final static  String DATABASE_IP = "ds139985.mlab.com";
	private final static int DATABASE_PORT = 39985;
	private final static String DBUSER = "rootins";
	private final static String DBPWD = "nchcTaiwan556";
	private final static String DBNAME = "nchc_ins";
	//"mongodb://rootins:apple123656@140.113.167.186/NCHC_INS"/
	private final static String URI = "mongodb://"+DBUSER+":"+DBPWD+"@"+DATABASE_IP+":"+DATABASE_PORT+"/"+DBNAME;
	//TODO: need to close the connection
	private final static MongoClient mongoClient = new MongoClient( new MongoClientURI(URI));
	private final static MongoDatabase mongodb = mongoClient.getDatabase(DBNAME);
	private static int total_device = 0;
	
	private MongoCollection<Document> collection;
	private static float[][] color_arr = new float[][]{
		{ 1.0f , 0.5f , 0.0f },  //Orange
		{ 0.372549f , 0.623529f , 0.623529f },  //CadetBlue
		{ 0.560784f , 0.560784f , 0.737255f },  //LightSteelBlue
		{ 0.858824f , 0.439216f , 0.858824f },  //Orchid
		{ 0.917647f , 0.678431f , 0.917647f }   //Plum 
		
	};
	private String name;
	final static int  head_off = SerialReader.head_off;
	JPanel panel = null;  //display information on on the pannel
	private int id;
	private int mheight;
	
	private int temp;
	private int battery_foot;
	private int battery_hand;
	private Logger log = App.log;
	private float color[];
	private long  updatetime;
	
	ArrayList<Point3d> arr_pos = null;//the position
	ArrayList<Integer>  arr_height = null;
	private int base_height;
	//private int OneFloorHeight = 3;
	private Date date; 
	
	static Point3d origin = new Point3d();
	final int floor_height = 10;
	StatusInfo(int device_id  /*,JPanel panel*/){
		this.id = device_id;
		//this.panel = panel;
		origin = new Point3d( 0 , 0 ,0 );
		color = new float[3];
		for( int i = 0 ; i < 3 ; i++ ){
			color[i] = color_arr[total_device%5][i];
		}
		total_device++;
		name = "Person "+total_device;  //for display on scene
		arr_pos = new ArrayList<Point3d>();
		arr_height = new ArrayList<Integer>();
		collection = mongodb.getCollection( "id"+String.valueOf(device_id ) );
		//updatetime = System.currentTimeMillis();
		date = new Date();
	}
	
	
	
	public void setid( int id){
		this.id = id;
	}
	
	public void setheight( int height ){
		this.mheight = height;
	}
	
	public void settemp( int temp ){
		this.temp = temp;
	}
	
	public void setbattery_foot( int battery ){
		this.battery_foot = battery;
	}
	
	public void setbattery_hand( int battery ){
		this.battery_hand = battery;
	}
	public void setupdateTume(){
		updatetime = System.currentTimeMillis();
	}
	public void addpos( double x , double y , double z){
		this.arr_pos.add( new Point3d( x, y, z ) );
	}
	
	public void addheight( int height ){
		this.arr_height.add( height );
	}
	
	public void setAllData( byte[] buffer , int length ){
		
		/*for( int i = 0 ; i < length ; i++){
			System.out.print( String.format("%02X", buffer[i])+" ");   			
		}
		*/
		
		ArrayList<Double> db_pos= new ArrayList<Double>();
		Document db_data = new Document();
		int device_id = ByteBuffer.wrap( buffer , 3+head_off, 4).order( ByteOrder.LITTLE_ENDIAN).getInt();
		int battery_hand = buffer[60+head_off];
		int battery_foot = buffer[61+head_off];
		int temp = buffer[62+head_off];
		int height = ( (buffer[64+head_off]&0xff) <<8 | buffer[65+head_off] & 0xff );
		if( battery_hand == 0 && battery_foot== 0){
			System.out.println("Battery is outof power");
			return;
		}
		setid(device_id);
		setbattery_foot(battery_foot);
		setbattery_hand(battery_hand);
		settemp(temp);
		setheight(height);
		
		int relative_height = 0;
		if( arr_height.isEmpty() ){  
			base_height = height;
		}	
		relative_height = ((height-base_height)/3 )*floor_height; 
		System.out.println("R H:"+relative_height);
		addheight(relative_height);
		
		for( int step = 0 ; step < 4 ; step++ ){
			
			float x = ByteBuffer.wrap(buffer , 12+12*step+head_off , 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
			float y = ByteBuffer.wrap(buffer , 16+12*step+head_off , 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
			float z = ByteBuffer.wrap(buffer , 20+12*step+head_off , 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
			//Point3d p1 = new Point3d( x , );
			
			
			if(  total_device == 1 && arr_pos.isEmpty()    ){ // set the program start in origin
				origin.set( x, z, y);
				System.out.println("*******Origin has set x:"+x+" y:"+y+" z:"+z);
			} 
			
			addpos( x-origin.x , z-origin.y , y-origin.z);  //the original y-axis maps to openGL z-axis
			//db_pos.add( (double) (Math.round(x-origin.x*1000 )/1000.0) );
			//db_pos.add( (double)relative_height );
			//db_pos.add( (double) (Math.round(y-origin.z*1000)/1000.0) );
			db_pos.add( (double) (Math.round((x-origin.x)*1000 )/1000.0) );
			db_pos.add( (double)relative_height );
			db_pos.add( (double) (Math.round((y-origin.z)*1000)/1000.0) );
			System.out.printf("step %d x:"+(x-origin.x)+" y: "+(z-origin.y)+" z: "+(y-origin.z)+"\n"   , step+1 );
			log.info("step " +(step+1) +" x:"+(x-origin.x)+" y: "+(z-origin.y)+" z: "+(y-origin.z)+"\n"  );
		}
		db_data.put( "ts" , new Date() );
		db_data.put( "foot", battery_foot);
		db_data.put( "hand", battery_hand);
		db_data.put( "pos", db_pos);
		db_data.put( "temp", temp);
		db_data.put( "height", height);
		
		collection.insertOne(db_data);
		//System.out.println( "H 1byte:"+buffer[64]);
		System.out.println( "Battery hand:"+battery_hand+" foot:"+battery_foot+", Temp: "+temp+"H: "+height );
		
	}
	
	public int getid(){
		return this.id;
	}
	
	public int gettemp(){
		return this.temp;
	}
	
	public int getheight(){
		return this.mheight;
	}
	
	public ArrayList<Point3d> getposlist(){
		return this.arr_pos;
	}
	public ArrayList<Integer> getheightlist(){
		return this.arr_height;
	}
	public int getbattery_foot(){
		return this.battery_foot;
	}
	public long getupdateTime(){
		return this.updatetime;
	}
	
	
	public int getbattery_hand(){
		return this.battery_hand;
	}
	public Point3d getorigin(){
		return origin;
	}
	public  float[] getColor(){
		return this.color;
	}
	public String getDevName(){
		return this.name;
	}
}
